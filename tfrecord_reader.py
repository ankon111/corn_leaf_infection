import os
import glob
import tensorflow as tf
import random
import imgaug.augmenters as iaa
from util import *


def random_jitter(crop):
    """ This function is used for add different augmentation to the image
    on a random basis.

    Args:
        crop: image to modify

    Returns:
        None

    """
    crop = tf.image.resize(crop, [256, 256],
                           method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    crop = tf.image.random_crop(crop, size=[height, width, 3])
    crop = tf.image.random_flip_up_down(crop)
    crop = tf.image.random_flip_left_right(crop)
    crop = tf.image.rot90(crop)
    noise = tf.random.normal(shape=tf.shape(crop), mean=0, stddev=0.02,
                             dtype=tf.float32)
    crop = tf.add(noise, crop)

    return crop


def aug_on_train_data(crop):
    """All of the training data is go through this basic augmentation process

    Args:
        crop: image to modify

    Returns:
        image
    """
    crop = crop.numpy()
    if random.random() < 0.5:
        sometimes = lambda aug: iaa.Sometimes(0.5, aug)
        seq = iaa.Sequential([sometimes(
            iaa.Affine(scale=(1.0, 1.1),
                       translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)},
                       rotate=(-20, 20),
                       shear=(-8, 8),
                       mode='edge'))])
        crop = seq.augment_image(crop)
    return crop


def img_augmentation(crop, label):
    """Responsible for all kinds of image augmentation process.

    Args:
        crop: image
        label: class

    Returns:

    """
    shape = crop.get_shape()
    crop = tf.py_function(aug_on_train_data, inp=[crop], Tout=tf.float32)
    crop.set_shape(shape)
    # choice = tf.random.uniform([], minval=0, maxval=1, dtype=tf.float32)
    # crop = tf.cond(choice < 0.5, lambda: crop, lambda: random_jitter(crop))
    return crop, label


class TFRecordReader:

    def __init__(self):
        self.data_dir = 'data'
        self.data_path = os.path.join(self.data_dir, 'records')
        self.seed = 10
        self.batch_size = 16
        self.buffer = 1000

    def parse_record(self, record):
        """This method is used for parsing the records e.g extracting features,
        labels.

        Args:
            record: tfrecord

        Returns:
            image, label

        """
        features = {
            'image': tf.io.FixedLenFeature([], dtype=tf.string),
            'height': tf.io.FixedLenFeature([], dtype=tf.int64),
            'width': tf.io.FixedLenFeature([], dtype=tf.int64),
            'label': tf.io.FixedLenFeature([], dtype=tf.int64),
        }
        record = tf.io.parse_single_example(record, features)
        img = tf.io.decode_raw(record['image'], tf.float32)
        img = tf.reshape(img, [record['height'], record['width'], 3])
        label = tf.one_hot(record['label'], len(get_labels()),
                           dtype=tf.float32)
        return img, label

    def generate_dataset(self, is_training=True):
        """Read tfrecord, parse the images and labels, do all the processing
        like different types of augmentation.

        Args:
            is_training: a boolean value

        Returns:
            processed dataset
        """
        is_shuffle = True
        if is_training:
            data_type = 'train'
            buffer = 1000
        else:
            #         is_shuffle = False
            data_type = 'validation'
            buffer = 100

        files = os.path.join(self.data_path,
                             '{}_*.tf_record'.format(data_type))
        filenames = glob.glob(files)
        dataset = tf.data.Dataset.list_files(files, shuffle=is_shuffle,
                                             seed=self.seed)
        dataset = dataset.interleave(lambda fn: tf.data.TFRecordDataset(fn),
                                     cycle_length=len(filenames),
                                     num_parallel_calls=min(len(filenames),
                                                            tf.data.experimental.AUTOTUNE))
        dataset = dataset.map(self.parse_record,
                              num_parallel_calls=tf.data.experimental.AUTOTUNE)
        if is_training:
            dataset = dataset.map(img_augmentation,
                                  num_parallel_calls=tf.data.experimental.AUTOTUNE)
            dataset = dataset.shuffle(buffer, seed=self.seed)
            dataset = dataset.repeat(1)
            dataset = dataset.prefetch(1)
        else:
            dataset = dataset.repeat(1)

        dataset = dataset.batch(self.batch_size)
        return dataset


if __name__ == '__main__':
    record_reader = TFRecordReader()
    train_dataset = record_reader.generate_dataset(is_training=True)
    for data in train_dataset.take(1):
        print(data)
