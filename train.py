import os
import random
import tensorflow as tf
import pickle
import datetime

# setting some GPU configs
from tensorflow_core.python.keras.backend import categorical_crossentropy
from tensorflow_core.python.keras.optimizers import Adam

from vgg_model import vgg_model
from tfrecord_reader import TFRecordReader

random.seed(1001)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
devices = tf.config.experimental.list_physical_devices('GPU')
# tf.config.experimental.set_memory_growth(devices[0], True)


def train_model(model, train_dataset, validation_dataset):
    """This method is used for training the model.

    Return:
        None
    """

    # Reading the class weights
    class_weight_dict = None
    with open("class_weights.pickle", "rb") as data:
        class_weight_dict = pickle.load(data)

    log_dir = os.path.join("log_data", datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))
    board = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

    history = model.fit(train_dataset, verbose=2, epochs=100, class_weight=class_weight_dict,
                        validation_data=validation_dataset, callbacks=[board])

    train_score = model.evaluate(train_dataset, verbose=0)
    print('train loss, train acc:', train_score)

    validation_score = model.evaluate(validation_dataset, verbose=0)
    print('validation loss, validation acc:', validation_score)

    model.save("corn_leaf_model_02.h5")

    return history


if __name__ == '__main__':
    # Train the model
    record_reader = TFRecordReader()
    train_dataset = record_reader.generate_dataset(is_training=True)
    validation_dataset = record_reader.generate_dataset(is_training=False)
    model = vgg_model()
    model.summary()
    model.compile(optimizer=Adam(learning_rate=0.0001), loss=categorical_crossentropy, metrics=["accuracy"])
    history = train_model(model, train_dataset=train_dataset, validation_dataset=validation_dataset)

    with open('corn_leaf_hist.pickle', 'wb') as hist_file:
        pickle.dump(history.history, hist_file)
