import pickle

if __name__ == '__main__':

    class_weight_dict = None
    with open("corn_leaf_hist.pickle", "rb") as data:
        class_weight_dict = pickle.load(data)

    print(class_weight_dict)