from typing import List


def get_labels() -> List:
    """This method is used to get all the labels name.

    Return:
        A list of label names.
    """
    #     labels = []
    #     for label_dir in _all_labels_dirs:
    #         labels.append(os.path.basename(label_dir))
    #     return sorted(labels)
    classes = ["healthy",
               "infected"
               ]
    return classes


def encode_label(label: str) -> int:
    if label == "healthy":
        return 1
    if label == "infected":
        return 0


output_class = len(get_labels())
data_name = 'corn_leaf'
height, width = 224, 224
