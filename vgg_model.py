import tensorflow as tf
from util import *


def vgg_model():
    """ This method is creating a vgg model using functional API.

    Returns:
        model
    """
    inputs = tf.keras.Input(shape=(height, width, 3))
    x = inputs

    x = tf.keras.layers.Conv2D(64, 3, padding="same", name="conv_1")(x)
    x = tf.keras.layers.ReLU(name="relu_1")(x)
    x = tf.keras.layers.BatchNormalization(name="bn_1")(x)

    x = tf.keras.layers.Conv2D(64, 3, padding="same", name="conv_2")(x)
    x = tf.keras.layers.ReLU(name="relu_2")(x)
    x = tf.keras.layers.BatchNormalization(name="bn_2")(x)
    x = tf.keras.layers.MaxPool2D(name="max_pool_1")(x)

    x = tf.keras.layers.Conv2D(128, 3, padding="same", name="conv_3")(x)
    x = tf.keras.layers.ReLU(name="relu_3")(x)
    x = tf.keras.layers.BatchNormalization(name="bn_3")(x)

    x = tf.keras.layers.Conv2D(128, 3, padding="same", name="conv_4")(x)
    x = tf.keras.layers.ReLU(name="relu_4")(x)
    x = tf.keras.layers.BatchNormalization(name="bn_4")(x)
    x = tf.keras.layers.MaxPool2D(name="max_pool_2")(x)

    x = tf.keras.layers.Conv2D(256, 3, padding="same", name="conv_5")(x)
    x = tf.keras.layers.ReLU(name="relu_5")(x)
    x = tf.keras.layers.BatchNormalization(name="bn_5")(x)

    x = tf.keras.layers.Conv2D(256, 3, padding="same", name="conv_6")(x)
    x = tf.keras.layers.ReLU(name="relu_6")(x)
    x = tf.keras.layers.BatchNormalization(name="bn_6")(x)
    x = tf.keras.layers.MaxPool2D(name="max_pool_3")(x)

    x = tf.keras.layers.Conv2D(512, 3, padding="same", name="conv_7")(x)
    x = tf.keras.layers.ReLU(name="relu_7")(x)
    x = tf.keras.layers.BatchNormalization(name="bn_7")(x)

    x = tf.keras.layers.Conv2D(512, 3, padding="same", name="conv_8")(x)
    x = tf.keras.layers.ReLU(name="relu_8")(x)
    x = tf.keras.layers.BatchNormalization(name="bn_8")(x)
    x = tf.keras.layers.MaxPool2D(name="max_pool_4")(x)

    x = tf.keras.layers.GlobalAveragePooling2D(name='gap')(x)

    x = tf.keras.layers.Dense(output_class, activation="softmax", name="dense")(x)

    base_model = tf.keras.Model(inputs, x, name='base_model')
    return base_model


if __name__ == '__main__':
    model = vgg_model()
    model.summary()
