import os
import glob
import shutil
import random
from tqdm import tqdm


class TrainTestSplit:

    def __init__(self):
        self.data_dir = 'data'
        self.image_dir = os.path.join(self.data_dir, 'images')
        self.train_dir = os.path.join(self.data_dir, 'train')
        self.test_dir = os.path.join(self.data_dir, 'test')
        self.validation_dir = os.path.join(self.data_dir, 'validation')
        self.val_ratio = 0.08
        self.test_ratio = 0.02

    def create_dir(self):
        """ This method is responsible for creating data directory and
        removing the existing dataset.

        Returns:

        """
        dir_list = [self.train_dir, self.test_dir, self.validation_dir]
        for d in dir_list:
            if os.path.exists(d):
                print("Removing existing {} directory and it's content...".format(d))
                shutil.rmtree(d)

            print("Creating {} directory....".format(d))
            os.mkdir(d)

    def create_train_val_test_dir(self):
        """ This method is responsible for create train, test, validation
        directory and split the data in a given ratio

        Args:
            dirs: list of directory

        Returns:

        """
        dir_list = [self.test_dir, self.validation_dir, self.train_dir]

        # temp_dir_name = os.path.basename(glob.glob(data_dir + '*')[0])
        classes = [os.path.basename(path) for path in
                   glob.glob(os.path.join(self.image_dir, '*'))]

        count = len(
            glob.glob(os.path.join(self.image_dir, classes[0], '*')))
        val_files_count = int(self.val_ratio * count)
        test_files_count = int(self.test_ratio * count)
        train_files_count = count - val_files_count - test_files_count
        file_count = [test_files_count, val_files_count, train_files_count]
        print("Total train files count: {}".format(train_files_count))
        print("Total validation files count: {}".format(val_files_count))
        print("Total test files count: {}".format(test_files_count))

        print("File copy started......")
        for idx, dir in enumerate(dir_list):
            for cls_idx, cls in enumerate(classes):
                dest_path = os.path.join(dir, cls)
                os.makedirs(dest_path)
                src_files = glob.glob(os.path.join(self.image_dir, cls, '*.jpg'))
                if idx != len(dir_list) - 1:
                    src_files = random.sample(src_files, len(src_files))
                    src_files = src_files[:file_count[idx]]
                print('File copying for {} class {}'.format(os.path.basename(dir),cls))
                for file in tqdm(src_files):
                    shutil.move(file, dest_path)

        print("File copy finished.")
        print("Removing {} directory".format(self.image_dir))
        shutil.rmtree(self.image_dir)

    def run(self):
        self.create_dir()
        self.create_train_val_test_dir()


if __name__ == '__main__':
    tt_split = TrainTestSplit()
    tt_split.run()
