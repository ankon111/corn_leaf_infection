# CORN LEAF INFECTION

### Create Environment
Let's create conda environment using `environment.yml` file.

creating conda environment:

```conda env create -f environment.yml```

updating conda environment:

```conda env update -f environment.yml```


### Dataset
Please download the dataset from [here](https://www.kaggle.com/qramkrishna/corn-leaf-infection-dataset)

### Run Configuration

First extract the dataset and create a directory called `data`. Then create a sub directory
called `images` inside of the `data` directory. Extract the dataset inside of the `images`
directory. So finally this will looks like this:

```
├───images
    ├───healthy
    └───infected
```

**Note:** This directory creation  will be code based from next release.

Then run the files accordingly:
1. `train_test_split`:  90% data used for train, 8% data for validation and rest is for testing.
    As name suggested this file is used for train test split.
2. `tfrecord_writer`: create `tfrecords` with features and labels.
3. `tfrecord_reader`: parse `tfrecords` with features and labels do some augmentation on training 
    data. This file is responsible for parsing and for creation of training and validation dataset.
4. `train`: This file is used for training and it prints the logs in console. Save the model and history
    for further analysis. 
5. `evaluation`: After training evaluation is done based on the saved model. This method helps to
    generate classification report and confusion matrix.

**Future Task:**

1. Class based code and docstring 
2. Update readme with results
3. train the data with other models
   
