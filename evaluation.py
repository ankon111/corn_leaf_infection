import os
import glob
import pickle

import cv2
import random
import numpy as np
from util import *
from tqdm import tqdm
import tensorflow as tf
import scikitplot as skplt
from tensorflow.keras.models import load_model
from sklearn.metrics import accuracy_score, classification_report, roc_auc_score, confusion_matrix

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
devises = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(devises[0], True)
random.seed(1001)


def load_test_data():
    x = []
    y = []
    test_img_dirs = glob.glob(os.path.join('data', 'test', '*'))
    for dir in test_img_dirs:
        count = 0
        label = encode_label(os.path.basename(dir))
        images = glob.glob(os.path.join(dir, '*'))
        for img in tqdm(images):
            if count == 10:
                break
            image = cv2.imread(img, cv2.IMREAD_COLOR)
            image = cv2.resize(image, (height, width)) / 255
            x.append(image)
            y.append(label)
            count = count + 1

    x = np.array(x, dtype=np.float32)
    y = np.array(y, dtype=np.int16())

    return x, y


if __name__ == '__main__':
    X_test, y_test = load_test_data()
    print(X_test.shape)
    model = load_model("corn_leaf_model_01.h5")

    """
    Prediction of probability and classes of test data. 
    """
    prediction_probability = model.predict(X_test)
    prediction_class = prediction_probability.argmax(axis=-1)

    """
    Classification report includes precision, recall, and F1 score.
    """
    accuracy = accuracy_score(y_test, prediction_class)
    print('Test Dataset Accuracy Score: %f' % accuracy)

    report = classification_report(y_test, prediction_class, target_names=['infected', 'healthy'])
    print(report)

    """
    Plot Confusion Matrix
    """

    # cm = confusion_matrix(y_test, prediction_class)
    # print("Confusion Matrix:")
    # print(cm)

    print("Plotting confusion matrix...")
    plot_CM = skplt.metrics.plot_confusion_matrix(y_test, prediction_class)
    plot_CM.figure.savefig("Confusion_Matrix")

    history = None
    with open('corn_leaf_hist.pickle', 'wb') as hist_file:
        history = pickle.load(hist_file)